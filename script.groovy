def incrementVersion() {
    echo "building the application..."
    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"

}

def buildJar() {
    echo "building jar file..."
    sh 'mvn clean package'
} 

def buildImage() {
    echo "building the docker image..."
    sh "docker build -t mohamedelsaghier/java-maven:${IMAGE_NAME} ."
} 

def deployApp() {
    echo 'deploying the application...'
     withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push mohamedelsaghier/java-maven:${IMAGE_NAME}"
    }
} 

def commitVersion() {
    echo "committing version update..."
    withCredentials([usernamePassword(credentialsId: 'gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        // git config here for the first time run
        sh 'git config --global user.email "mohamed@jenkins.com"'
        sh 'git config --global user.name "jenkins"'

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/mohamedelsaghier/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:master'
    }
} 

return this
